# This makefile contains no information about file structure or tool locations.

# Artifact names:
export PRODUCT_NAME := Java Utilities


################################################################################
# Tasks


.PHONY: all mvnversion jars test javadoc clean info

all: test install javadoc

mvnversion:
	mvn --version


# ------------------------------------------------------------------------------
# Package into Jar files and install in local maven repository.
install:
	mvn clean install


# ------------------------------------------------------------------------------
# For development.
test:
	mvn test


# ------------------------------------------------------------------------------
# Generate javadocs for all modules.
javadoc:
	mvn javadoc:javadoc


# ------------------------------------------------------------------------------
clean:
	mvn clean

info:
	@echo "Makefile for $(PRODUCT_NAME)"
