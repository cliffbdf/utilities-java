package cliffberg.utilities;

public class AssertionUtilities {
	
	/**
	 If expr is false, print the message and throw an Exception.
	 */
	public static void assertThat(boolean expr, String msg) {
		if (msg == null) msg = "";
		if (msg != null) msg = "; " + msg;
		if (! expr) throw new RuntimeException("Assertion violation: " + msg);
	}
	
	/**
	 If the node is not class c, throw an exception.
	 */
	public static void assertIsA(Object n, Class c) {
		if (n == null) throw new RuntimeException("Object is null");
		if (c == null) throw new RuntimeException("Class is null");
		if (! c.isAssignableFrom(n.getClass())) throw new RuntimeException(
			"Assertion violation: object is a " + n.getClass().getName());
	}
	
	/**
	 If expr is false, perform the specified action and then throw an Exception.
	 */
	public static void assertThat(boolean expr, Runnable action) {
		if (! expr) {
			System.out.println("Assertion violation; performing diagnostic action...");
			try {
				action.run();
			} finally {
				System.out.println();
				System.out.println("End of diagnostic action.");
			}
			throw new RuntimeException("Assertion violation");
		}
	}
}
