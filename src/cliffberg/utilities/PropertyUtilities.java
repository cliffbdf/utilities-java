package cliffberg.utilities;

import java.util.Properties;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class PropertyUtilities {
	
	/**
	 Obtain a configuration setting. Obtain it first from the environment;
	 if not found, then look for a properties file in the current directory
	 and then in the user's home directory. If that fails, look for the properties
	 file in the classpath. If that fails, return null.
	 */
	public static String getSetting(String name, String propertyFileName) throws Exception {
		
		// Check environment.
		String value = System.getenv(name);
		if (value != null) return value;
		
		// Check current directory for a properties file.
		Properties curDirProperties = getCurDirProperties(propertyFileName);
		if (curDirProperties != null) {
			value = curDirProperties.getProperty(name);
			if ((value != null) && (! value.equals(""))) return value;
		}
		
		// Check user's home directory for a properties file.
		Properties homeProperties = getUserHomeProperties(propertyFileName);
		if (homeProperties != null) {
			value = homeProperties.getProperty(name);
			if ((value != null) && (! value.equals(""))) return value;
		}
		
		// Check classpath for a properties file.
		Properties classpathProperties = getClasspathProperties(propertyFileName);
		if (classpathProperties != null) {
			value = classpathProperties.getProperty(name);
			if ((value != null) && (! value.equals(""))) return value;
		}
		
		// Setting was not found.
		return null;
	}
	
	/**
	 Assemble a map of the container runtime properties. They may be specified in all
	 of the same manner as other properties, with the same precedence rules.
	 */
	public static Properties getContainerProperties(String containerPropertyFileName) throws Exception {
		
		Properties properties = new Properties();
		
		// Load settings from the classpath first - these have the lowest precedence.
		Properties classpathProperties = getClasspathProperties(containerPropertyFileName);
		if (classpathProperties != null) properties.putAll(classpathProperties);
		
		// Next load settings from the user home directory.
		Properties homeProperties = getUserHomeProperties(containerPropertyFileName);
		if (homeProperties != null) properties.putAll(homeProperties);
		
		// Finally, load settings from the current directory - these have the
		// highest precedence.
		Properties curDirProperties = getCurDirProperties(containerPropertyFileName);
		if (curDirProperties != null) properties.putAll(curDirProperties);
		
		return properties;
	}
	
	protected static Properties getCurDirProperties(String propertyFileName) throws Exception {
		
		String dirstr = System.getProperty("user.dir");
		if (dirstr == null) throw new RuntimeException("No current working directory");
		File curdir = new File(dirstr);
		if (! curdir.exists()) throw new RuntimeException("Current directory not found");
		File curdirPropertyFile = new File(curdir, propertyFileName);
		if (curdirPropertyFile.exists()) {
			Properties properties = new Properties();
			properties.load(new FileReader(curdirPropertyFile));
			return properties;
		}
		return null;
	}
	
	protected static Properties getUserHomeProperties(String propertyFileName) throws Exception {
		
		String homestr = System.getProperty("user.home");
		if (homestr == null) throw new RuntimeException("No user home");
		File home = new File(homestr);
		if (! home.exists()) throw new RuntimeException("User home directory not found");
		File homePropertyFile = new File(home, propertyFileName);
		if (homePropertyFile.exists()) {
			Properties properties = new Properties();
			properties.load(new FileReader(homePropertyFile));
			return properties;
		}
		return null;
	}
	
	protected static Properties getClasspathProperties(String propertyFileName) {
		
		try {
			Properties properties = new Properties();
			InputStream is = PropertyUtilities.class.getResourceAsStream("/" + propertyFileName);
			if (is == null) return null;
			properties.load(is);
			return properties;
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
}
